
Development environment for the Tryton application platform.

This project contains our custom git based tool set, thanks to NaN-tic for providing the original set.

Installing
----------

Clone this module into your Python virtualenv.


Install the requirements with 

    pip install -r requirements.txt


Have a look at the available taks with

    invoke -l


Do the full bootstrap of the environment with

    invoke bs



Support
-------

For more information or if you encounter any problems with this module,
please contact the programmers at

#### MBSolutions

   * Issues:   https://gitlab.com/m9s/tryton-tasks/issues
   * Website:  http://www.m9s.biz/
   * Email:    info@m9s.biz

If you encounter any problems with Tryton, please don't hesitate to ask
questions on the Tryton bug tracker, mailing list, wiki or IRC channel:

   * http://bugs.tryton.org/
   * http://groups.tryton.org/
   * http://wiki.tryton.org/
   * irc://irc.freenode.net/tryton

License
-------

See LICENSE

Copyright
---------

See COPYRIGHT

